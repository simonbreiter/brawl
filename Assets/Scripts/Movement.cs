﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {

	public float speed = 5.0f;
	public float gravity = 20.0f;
	public float deadzone = 0.25f;
	public float turnSlerpSpeed = 10f;

	private int playerID;
	private Vector3 moveDirection;
	private Vector3 fireDirection;
	private GameObject top;
	private ParticleSystem smoke;


	void Awake() {
		playerID = GetComponent<Player>().playerID;
		top = GameObject.Find (string.Format ("Player P{0}(Clone)/Top", playerID));
		smoke = GameObject.Find (string.Format ("Player P{0}(Clone)/Smoke", playerID)).GetComponent<ParticleSystem>();
	}

	void Update () {
//		Debugging
//		print ("Horizontal P" + playerID.ToString());
//		print ("Vertical P" + playerID.ToString());

		// TODO: Why is this controller assigned in the update method?
		// Shouldn't it be in the awake methode once?
		CharacterController controller = GetComponent<CharacterController>();

		// Movement
		// Determine where the player is heading
		moveDirection = new Vector3(Input.GetAxis("Horizontal P" + playerID.ToString()), 0, 
		                            Input.GetAxis("Vertical P" + playerID.ToString()));
		// Deadzone
//		if(lookDirection.magnitude < deadzone) {
//			lookDirection = Vector3.zero;
//		}
		if (controller.isGrounded) {
			// Only turn vehicle when vector is slightly bigger then zero
			// prevent the "snapping back" this way, also slerp between the old and 
			// new rotation for a smooth turn of the turret
			if(moveDirection.sqrMagnitude > 0.1f) {
				transform.rotation = Quaternion.Slerp(transform.rotation, 
				                                      Quaternion.LookRotation(moveDirection), 
				                                      Time.deltaTime * turnSlerpSpeed);
			}
			Debug.DrawRay(transform.position, moveDirection, Color.green);
			smoke.Play();
		} else {
			// TODO: I'am still not satisfied how the gravity is applied, it feels clunky
			moveDirection.y -= gravity * Time.deltaTime;
		}

		// TODO: Moved this part to the weapon script, this has nothing to do with movement
		// Test this and this part afterwards if tests are successfull
//		// Firing
//		fireDirection = new Vector3(Input.GetAxis("3th Axis"), 0, Input.GetAxis("4th Axis"));
//		// Only turn weapon when vector is slightly bigger then zero
//		// prevent the "snapping back" this way, also slerp between the old and new rotation for a smooth turn
//		if(fireDirection.sqrMagnitude > 0.1f) {
//			top.transform.rotation = Quaternion.Slerp(top.transform.rotation, 
//			                                          Quaternion.LookRotation(fireDirection), 
//			                                          Time.deltaTime * turnSlerpSpeed);
//		}

		// If vehicle dont move, dont render smoke
		if(moveDirection.Equals(Vector3.zero)) {
			smoke.Stop();
		}

		// TODO: Maye move this up in the isGrounded block to prevent moving while in air?
		controller.Move(moveDirection * speed * Time.deltaTime);

		// Debug stuff
//		print ("Move Direction: " + moveDirection);
//		print ("Fire direction: " + fireDirection);
//		print ("Direction: " + 	Quaternion.identity);
//		print ("Position" + transform.position);
//		print ("Euler angles: " + transform.eulerAngles);

	}

	void OnControllerColliderHit(ControllerColliderHit hit) {
		// Gets rigidbody of the gameobject that was hit
		Rigidbody body = hit.collider.attachedRigidbody;

		if (body == null || body.isKinematic)
			return;
		
//		if (hit.moveDirection.y < -0.3F)
//			return;

		body.velocity = speed * moveDirection;
	}

}
