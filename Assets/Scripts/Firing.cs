﻿using UnityEngine;
using System.Collections;

public class Firing : MonoBehaviour {

	public float turnSlerpSpeed = 10f;

	private int playerID;
	private Vector3 moveDirection;
	private Vector3 fireDirection;
	private GameObject top;
	private GameObject cannon;
	private GameObject machineGun;

	void Awake () {
		playerID = GetComponent<Player>().playerID;
		top = GameObject.Find (string.Format ("Player P{0}(Clone)/Top", playerID));
		cannon = GameObject.Find (string.Format ("Player P{0}(Clone)/Top/Cannon", playerID));
		machineGun = GameObject.Find (string.Format ("Player P{0}(Clone)/Top/MachineGun", playerID));
	}

	void Update () {
//		Debugging
//		print ("3th Axis P" + playerID.ToString());
//		print ("4th Axis P" + playerID.ToString());

		// Firing
		fireDirection = new Vector3(Input.GetAxis("3th Axis P" + playerID.ToString()), 0, 
		                            Input.GetAxis("4th Axis P" + playerID.ToString()));
		// Only turn weapon when vector is slightly bigger then zero
		// prevent the "snapping back" this way, also slerp between the old and new rotation for a smooth turn
		if(fireDirection.sqrMagnitude > 0.1f) {
			top.transform.rotation = Quaternion.Slerp(top.transform.rotation, 
			                                          Quaternion.LookRotation(fireDirection), 
			                                          Time.deltaTime * turnSlerpSpeed);
		}

		// Canon
		if(Input.GetButton("Fire1 P" + playerID.ToString())) {
			cannon.SendMessage("Fire", SendMessageOptions.DontRequireReceiver);
		}

		// Machine gun
		if(Input.GetButton("Fire2 P" + playerID.ToString())) {
			machineGun.SendMessage("Fire", SendMessageOptions.DontRequireReceiver);
		}
	
	}
}
