﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour {

	public Rigidbody projectile;
	public Rigidbody shell;
//	public float bulletVelocity = 50f;
//	public float shellVelocity = 10f;
	public float fireRate = 0.5f;
	public float recoil = 5;
	public float turnSlerpSpeed = 10f;
	public AudioSource shot;

	private float nextFire = 0.0f;
	private Vector3 moveDirection;
	private Vector3 fireDirection;
	private GameObject top;
	private GameObject bulletStartPos;
	private GameObject shellStartPos;
	private ParticleSystem smoke;
	private ParticleSystem muzzleFlash;

	void Awake() {
		top = GameObject.Find("Top");
		bulletStartPos = GameObject.Find("BulletStartPosition");
		shellStartPos = GameObject.Find ("ShellStartPosition");
		smoke = GameObject.Find ("Smoke").GetComponent<ParticleSystem>();
//		muzzleFlash = GameObject.Find ("MuzzleFlash").GetComponent<ParticleSystem>();
	}

	void Update () {

		// Firing
		fireDirection = new Vector3(Input.GetAxis("3th Axis"), 0, Input.GetAxis("4th Axis"));
		// Only turn weapon when vector is slightly bigger then zero
		// prevent the "snapping back" this way, also slerp between the old and new rotation for a smooth turn
		if(fireDirection.sqrMagnitude > 0.1f) {
			top.transform.rotation = Quaternion.Slerp(top.transform.rotation, 
			                                          Quaternion.LookRotation(fireDirection), 
			                                          Time.deltaTime * turnSlerpSpeed);
		}

		if(Input.GetButton("Fire1") && Time.time > nextFire) {

			// Shake camera when fire
			GameObject mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
			mainCamera.SendMessage("Shake", SendMessageOptions.DontRequireReceiver);
			shot.Play();

//			TODO: Test recoil with ps3 controller
//			TODO: The weapon should send e message to the player if fired, then it should be moved
//			in the playerMovement class
//			transform.parent.Translate(Vector3.back * Time.deltaTime * recoil);

			nextFire = Time.time + fireRate;

			// TODO: Create a projectile class and instantiate that
			// Instantiate a projectile and play muzzle flash
			Rigidbody projectileClone;
			projectileClone = Instantiate(projectile, bulletStartPos.transform.position, 
			                              bulletStartPos.transform.rotation) as Rigidbody;
//			muzzleFlash.Play();

			// Instantiate a new shell clone
			GameObject shellClone = Instantiate(shell, shellStartPos.transform.position, 
			                                    shellStartPos.transform.rotation) as GameObject;
		}
	
	}
}
