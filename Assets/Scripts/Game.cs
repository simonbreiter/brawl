﻿using UnityEngine;
using System.Collections;

public class Game : MonoBehaviour {

	// How many players are in the game
	public int players = 1;

	// Use this for initialization
	void Awake() {
		// For each player instantiate a player object
		for (int i = 0; i < players; i++) {
			// Load prefab
			GameObject player = (GameObject)Resources.Load ("GameObjects/Player/Prefabs/Player");
			// Set playerID
			int playerID = i + 1;
			player.GetComponent<Player>().playerID = playerID;
			// Set name
			player.name = "Player P" + playerID;
			// Instantiate
			Instantiate (player);
		}
	}
}
