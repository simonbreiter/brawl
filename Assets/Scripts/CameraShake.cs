﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour {

	public float shakeDecayMultiplier = 0.002f;
	public float shakeIntensityMultiplier = 0.3f;

	private Vector3 originPosition;
	private Quaternion originRotation;
	private float shakeDecay;
	private float shakeIntensity;

	private void Update(){
		if(shakeIntensity > 0){
			transform.position = originPosition + Random.insideUnitSphere * shakeIntensity;
			transform.rotation =  new Quaternion(
				originRotation.x + Random.Range(-shakeIntensity, shakeIntensity) * 0.2f,
				originRotation.y + Random.Range(-shakeIntensity, shakeIntensity) * 0.2f,
				originRotation.z + Random.Range(-shakeIntensity, shakeIntensity) * 0.2f,
				originRotation.w + Random.Range(-shakeIntensity, shakeIntensity) * 0.2f);
			shakeIntensity -= shakeDecay;
		}
	}
	
	private void Shake(){
		originPosition = transform.position;
		originRotation = transform.rotation;
		shakeIntensity = shakeIntensityMultiplier;
		shakeDecay = shakeDecayMultiplier;
	}
}
