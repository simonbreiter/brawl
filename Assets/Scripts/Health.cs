﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {

	public int health = 20;

	public void ApplyDamage(int damage) {
		health -= damage;
		if(health <= 0) {
			gameObject.SendMessage("Explode", SendMessageOptions.DontRequireReceiver);
		}
		print ("Health: " + health);
	}
}
