﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	public int playerID = 1;
	public int health = 100;

	public void Respawn () {
			print ("Player Health: " + health);
			gameObject.SendMessage("ExplodeDontDestroy", SendMessageOptions.DontRequireReceiver);
			// Send message to
			gameObject.SendMessage("Inactivate", SendMessageOptions.DontRequireReceiver);
			// Send Message to respawner
			// Respawner sets gameobject to new position and set it active again
			gameObject.SendMessage("SpawnAtRandomSpawnPoint", SendMessageOptions.DontRequireReceiver);
			health = 100;
	}

	public void ApplyDamage (int damage) {
		health -= damage;
		if(health <= 0) {
			Respawn ();
		}
	}

}
