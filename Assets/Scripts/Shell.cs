using UnityEngine;
using System.Collections;

public class Shell : MonoBehaviour {

	public float shellVelocity = 10f;
//	public AudioSource shellSound;

	private GameObject shellStartPos;
	private Rigidbody shell;

	void Awake() {
		shell = gameObject.GetComponent<Rigidbody>();
		shellStartPos = GameObject.Find ("ShellStartPosition");
	}

	// Use this for initialization
	void Start () {
//		shellSound.Play();
		shell.velocity = transform.TransformDirection(Vector3.forward * shellVelocity);
		StartCoroutine(disablePhysics(shell));
		StartCoroutine(destroyRandomShell(shell));
	}

	IEnumerator disablePhysics(Rigidbody shellClone) {
		yield return new WaitForSeconds(3);
		shellClone.isKinematic = true;
		shellClone.GetComponent<BoxCollider>().enabled = false;
	}

	IEnumerator destroyRandomShell(Rigidbody shellClone) {
		yield return new WaitForSeconds(5);
		if(Random.Range(0,10) > 5) {
			Destroy(shellClone.gameObject);
		}
	}

}
