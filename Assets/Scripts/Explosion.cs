﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {

	// TODO: Depreciated, delete after refactoring

	private Component[] particles;

	IEnumerator destroyObject() {
		gameObject.GetComponent<MeshRenderer>().enabled = false;
		gameObject.GetComponent<BoxCollider>().enabled = false;
		yield return new WaitForSeconds(2);
		Destroy(gameObject);
	}

	private void explode() {
		particles = GetComponentsInChildren<ParticleSystem>();
		foreach (ParticleSystem particle in particles) {
			particle.Play();
		}
		StartCoroutine("destroyObject");

		GameObject mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
		mainCamera.SendMessage("Shake", SendMessageOptions.DontRequireReceiver);
	}    
}
