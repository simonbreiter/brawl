﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

	// Velocity of bullet
	public float bulletVelocity = 20.0f;
	// Damage of bullet
	public int damage = 10;
	// Camera shake when bullet explodes
//	public int cameraShake = 1;

	private bool canMove = true;

	void Start() {
		// After 3 seconds destroy object
		Destroy(this.gameObject, 3);
	}

	void FixedUpdate() {
		if (canMove) {
			transform.Translate(transform.up * bulletVelocity * Time.deltaTime, Space.World);
		}
	}

	void OnTriggerEnter(Collider other) {
		// If projectile hits something, stop it
		transform.Translate(new Vector3(0,0,0));
		canMove = false;
		// Disable collider
		GetComponent<Collider>().enabled = false;
		// Disable mesh
		GetComponent<MeshRenderer>().enabled = false;

		Component[] particles = GetComponentsInChildren<ParticleSystem>();
		foreach (ParticleSystem particle in particles) {
			particle.Stop();
		}

		// Apply Damage
		other.gameObject.SendMessage("DamageObject", SendMessageOptions.DontRequireReceiver);
		other.gameObject.SendMessage("ApplyDamage", damage, SendMessageOptions.DontRequireReceiver);

		// Shake camera
		GameObject mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
		mainCamera.SendMessage("Shake", SendMessageOptions.DontRequireReceiver);
	}

}
