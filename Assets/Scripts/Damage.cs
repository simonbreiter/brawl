﻿using UnityEngine;
using System.Collections;

public class Damage : MonoBehaviour {

	public GameObject scraps;
	public int scrapsCount;
	public float velocity;

	private Component[] particles;

	IEnumerator InstantiateScraps() {
		GameObject[] objCreated = new GameObject[scrapsCount];
		for(int i = 0; i < scrapsCount; i++) {
			yield return new WaitForSeconds(0.01f);
			objCreated[i] = Instantiate (scraps, transform.position, Random.rotation) as GameObject;    
			objCreated[i].GetComponent<Rigidbody>().velocity = transform.TransformDirection(objCreated[i].transform.forward * velocity);
		}
	}

	IEnumerator DestroyObject() {
		gameObject.GetComponent<MeshRenderer>().enabled = false;
		gameObject.GetComponent<BoxCollider>().enabled = false;
		yield return new WaitForSeconds(2);
		Destroy(gameObject);
	}
	
	private void DamageObject() {
		StartCoroutine("InstantiateScraps");
	}

	private void Inactivate() {
		Component[] allRenderers = gameObject.GetComponentsInChildren<MeshRenderer>();
		foreach (var renderer in allRenderers) {
			renderer.GetComponent<Renderer>().enabled = false;
		}
	}

	private void Activate() {
		Component[] allRenderers = gameObject.GetComponentsInChildren<MeshRenderer>();
		foreach (var renderer in allRenderers) {
			renderer.GetComponent<Renderer>().enabled = true;
		}
	}

	private void ExplodeDontDestroy() {
		print ("Bam!");
		particles = GetComponentsInChildren<ParticleSystem>();
		foreach (ParticleSystem particle in particles) {
			particle.Play();
			print ("Woop");
		}
		GameObject mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
		mainCamera.SendMessage("Shake", SendMessageOptions.DontRequireReceiver);
	}

	private void Explode() {
		particles = GetComponentsInChildren<ParticleSystem>();
		foreach (ParticleSystem particle in particles) {
			particle.Play();
		}
		StartCoroutine("DestroyObject");

		GameObject mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
		mainCamera.SendMessage("Shake", SendMessageOptions.DontRequireReceiver);
	}    
}