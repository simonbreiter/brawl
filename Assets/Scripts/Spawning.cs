﻿using UnityEngine;
using System.Collections;

public class Spawning : MonoBehaviour {

	private GameObject[] respawnPoints;

	void Awake() {
		respawnPoints = GameObject.FindGameObjectsWithTag("Respawn");
	}

	// Use this for initialization
	void Start () {
		foreach (var respawnPoint in respawnPoints) {
			print ("SpawnPoint initialized");
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator WaitUntilRespawning() {
		yield return new WaitForSeconds(2);
		int randomSpawnPoint = Random.Range(0, respawnPoints.Length);
		print ("Spawning at position " + randomSpawnPoint);
		transform.position = respawnPoints[randomSpawnPoint].transform.position;
		gameObject.SendMessage("Activate", SendMessageOptions.DontRequireReceiver);
	}

	public void SpawnAtRandomSpawnPoint() {
		StartCoroutine("WaitUntilRespawning");
	}
}
