﻿using UnityEngine;
using System.Collections;

public class PlayerColor : MonoBehaviour {

	public Material[] colors;

	private int playerID;

	void Start () {
		playerID = GetComponent<Player>().playerID;
		GetComponent<Renderer>().material = colors[playerID - 1];
	}
}
