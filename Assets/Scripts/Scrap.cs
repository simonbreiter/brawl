﻿using UnityEngine;
using System.Collections;

public class Scrap : MonoBehaviour {

	IEnumerator DisablePhysics() {
		yield return new WaitForSeconds(3);
		gameObject.GetComponent<Rigidbody>().isKinematic = true;
		gameObject.GetComponent<BoxCollider>().enabled = false;
	}

	void Start () {
		StartCoroutine("DisablePhysics");
	}
}
