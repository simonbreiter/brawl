﻿using UnityEngine;
using System.Collections;

public class MachineGun : MonoBehaviour {

	public Rigidbody projectile;
	public Rigidbody shell;
	public float fireRate = 0.3f;
	public float recoil = 5;
	public AudioSource shootingSound;

	private int playerID;
	private float nextFire = 0.0f;
	private GameObject bulletStartPos;
	private GameObject shellStartPos;

	void Awake() {
		playerID = transform.root.GetComponent<Player>().playerID;
		bulletStartPos = GameObject.Find(string.Format ("Player P{0}(Clone)/Top/MachineGun/ProjectileStartPosition", playerID));
		shellStartPos = GameObject.Find(string.Format ("Player P{0}(Clone)/Top/MachineGun/ShellStartPosition", playerID));
	}

	void Fire() {
		if(Time.time > nextFire) {
			print ("Firing machine gun");
			// Shake camera when fire
			GameObject mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
			mainCamera.SendMessage("Shake", SendMessageOptions.DontRequireReceiver);
			shootingSound.Play();
			
			//			TODO: Test recoil with ps3 controller
			//			TODO: The weapon should send e message to the player if fired, then it should be moved
			//			in the playerMovement class
			//			transform.parent.Translate(Vector3.back * Time.deltaTime * recoil);
			
			nextFire = Time.time + fireRate;
			
			// TODO: Create a projectile class and instantiate that
			// Instantiate a projectile and play muzzle flash
			Rigidbody projectileClone;
			projectileClone = Instantiate(projectile, bulletStartPos.transform.position, 
			                              bulletStartPos.transform.rotation) as Rigidbody;

			// Instantiate a new shell clone
			GameObject shellClone = Instantiate(shell, shellStartPos.transform.position, 
			                                    shellStartPos.transform.rotation) as GameObject;
		}
	}

}
